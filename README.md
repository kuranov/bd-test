# COVID Statistics

_Using DynamicTable, DynamicForms, AbstractControl_

This project provides possibility to view real daily COVID-2019 statistics from 22 Jan 2020 until now. 

Application gets daily data from https://github.com/CSSEGISandData/COVID-19 in CSV format, and transforms 
it to JSON-array in runtime.

With app interface you can:

* change current date using date picker control; 
* filter countries by name using filter-button in the top right corner of the table. 

## Install modules for web server

**First of all**, go to the `/backend/` directory and run `npm i`

I know, that it is not a perfect solution, but api proxy for translating data from CSV to JSON has their own _package.json_
and modules that should be installed separately of angular application. 


## How to launch

1. Run `npm run start:backend` to launch node.js micro-server (be sure, that port `8080` is not buzy), which resposible for fetching data from [data source](https://github.com/CSSEGISandData/COVID-19/tree/master/csse_covid_19_data/csse_covid_19_daily_reports).
1. Run `npm run start:frontend` to launch Angular application

## How to contact
Feel free to contact with me by email