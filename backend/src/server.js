const parse = require('csv-parse/lib/sync');
const {Application} = require('./common/application');
const {CovidStatisticsApi} = require('./common/covid-statistics-api');

const SOURCE_TABLES_URL = 'https://github.com/CSSEGISandData/COVID-19/raw/master/csse_covid_19_data/csse_covid_19_daily_reports/';

const statsApi = new CovidStatisticsApi(SOURCE_TABLES_URL);
const app = new Application();

app.route('', (req, res) => {
    res.end(JSON.stringify({
        result: "Welcome to COVID-2019 Reports API!"
    }));
});

app.route('countries', (req, res) => {
    const query = Application.getQuery(req);
    let date = new Date();

    if (query && query.date) {
        if (!isNaN(Date.parse(query.date))) {
            date = new Date(Date.parse(query.date));
        } else {
            onReportError('incorrect_date');
        }
    }

    const onReportLoaded = (csvText) => {
        let collection = parse(csvText, {
            skip_empty_lines: true,
        })
        .map(row => {
            let [state, country, lastUpdate, confirmed, deaths, recovered, lat, lon] = row;
            [confirmed, deaths, recovered] = [confirmed, deaths, recovered].map(v => parseInt(v, 10));
            [lat, lon] = [lat, lon].map(v => parseFloat(v));
            return {state, country, lastUpdate, confirmed, deaths, recovered, lat, lon};
        })
        .filter((el, index) => index > 0)
        .reduce((acc, item, idx) => {
          if (acc[item.country]) {
            acc[item.country].confirmed += item.confirmed;
            acc[item.country].deaths += item.deaths;
            acc[item.country].recovered += item.recovered;
          } else {
            acc[item.country] = item;
            delete acc[item.country].state;
          }
          return acc;
        }, {});
        collection = Object.values(collection).sort((a, b) => a.country > b.country ? 1 : -1);
        res.end(JSON.stringify(collection));
    };

    const onReportError = (error) => {
        // console.log(error);
        res.statusCode = 500;
        res.end(JSON.stringify([]));
    };

    statsApi.loadDailyReport(date)
        .then((result) => onReportLoaded(result))
        .catch((error) => onReportError(error));
});
