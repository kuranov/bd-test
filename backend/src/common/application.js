const http = require('http');
const url = require('url');

class Application {
    static getQuery(req) {
        return req.url && url.parse(req.url, true).query;
    }

    constructor(host = 'localhost', port = 8080) {
        this._routeHandlers = {};
        this.port = port;
        this.host = host;
        this._createServer();
        this.run();
    }

    run() {
        this.server.listen(this.port, this.host, () => {
            console.log(`COVID-2019 Statistics server API running at http://${this.host}:${this.port}/`);
        });
    }

    stop() {
        // this.server
    }

    route(path, func) {
        this._routeHandlers[path.replace('/', '')] = func;
        return this;
    }

    _createServer() {
        this.server = http.createServer((req, res) => {
            res.setHeader('Content-Type', 'application/json');
            res.setHeader('Access-Control-Allow-Origin', '*');
            if(!this._invokeAction(req, res)) {
                this._404(res);
            }
        });
    }

    _404(res) {
        res
            .writeHead(404, 'Not found')
            .end(JSON.stringify({}));
    }

    _invokeAction(req, res) {
        let atLeastOneInvoked = false;
        for(const path of Object.keys(this._routeHandlers)) {
            if (this._validateRoute(req, path)) {
                this._routeHandlers[path](req, res);
                atLeastOneInvoked = true;
            }
        }
        return atLeastOneInvoked;
    }

    _validateRoute(req, route) {
        if (!req || !req.url) {
            return false
        }
        let reqPath = url.parse(req.url, true).pathname;
        reqPath = this._trimSpacesAndSlashes(reqPath);
        reqPath = this._trimPagination(reqPath);
        return route === reqPath;
    }

    _trimSpacesAndSlashes(path) {
      return path.replace(/^\/|\/$/g, '');
    }

    _trimPagination(path) {
      return path.replace(/\/[0-9]+\/[0-9]+$/, '');
    }
}

exports.Application = Application;
