const rp = require('request-promise-native');

class CovidStatisticsApi {
    constructor(apiUrl) {
        this.apiUrl = apiUrl;
    }

    getUrlForDate(date) {
        return this.apiUrl + this.formatDate(date) + '.csv';
    }

    formatDate(date = new Date()) {
        return [date.getMonth() + 1, date.getDate(), date.getFullYear()]
            .map((el) => (el.toString().length % 2 === 1 ? '0' + el : el))
            .join('-');
    }

    loadDailyReport(date) {
        const url = this.getUrlForDate(date);
        console.log('load from url', url);
        return rp.get(url);
    }
}

exports.CovidStatisticsApi = CovidStatisticsApi;
