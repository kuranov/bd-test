import {Injectable} from '@angular/core';
import {CrudService} from '@bd-innovations/abstract-section';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})

export class CovidStatisticsService extends CrudService<Country> {
    constructor(public http: HttpClient) {
        super(http, {
            apiPrefix: '/api',
            apiPath: {
                getOne: 'countries',
                getMany: 'countries',
                post: 'countries',
                put: 'countries',
                delete: 'countries'
            }
        });
    }
}

export interface Country {
    state: string;
    country: string;
    lastUpdate: string;
    confirmed: number;
    deaths: number;
    recovered: number;
    lat: number;
    lon: number;
}