import {Component, OnInit} from '@angular/core';
import {PaginatedListLoaderConfigurator, RoutedComponentFasade} from '@bd-innovations/abstract-section';
import {Country, CovidStatisticsService} from '../providers/covid-statistics.service';
import {formatDate} from '@angular/common';
import {ColumnsV2Config, PaginationRequestModel} from '@bd-innovations/dynamic-tables';
import {PaginatedRequestConfig} from '@bd-innovations/abstract-section/lib/abstract-section-v2/configs/paginated-request.config';
import {DynamicFormService, GroupQuestion} from '@bd-innovations/dynamic-form';
import {FormGroup} from '@angular/forms';
import {pipe} from 'rxjs';

// Previous day date in one line
const yesterday = () => (d => new Date(d.setDate(d.getDate() - 1)))(new Date());

@Component({
    selector: 'app-covid-statistics-table',
    templateUrl: './covid-statistics-table.component.html',
    styleUrls: ['./covid-statistics-table.component.sass']
})
export class CovidStatsTableComponent extends RoutedComponentFasade<Country> implements OnInit {
    public data: Country[] = [];

    public columns: ColumnsV2Config = {
        Country: {
            discriminator: 'TextColumn',
            paths: 'country',
            control: {
                discriminator: 'textbox',
                type: 'text',
                label: 'Country name'
            }
        },
        Confirmed: {
            discriminator: 'TextColumn',
            paths: 'confirmed'
        },
        Death: {
            discriminator: 'TextColumn',
            paths: 'deaths'
        },
        Recovered: {
            discriminator: 'TextColumn',
            paths: 'recovered'
        },
    };

    public filtersQuestion: GroupQuestion = {
        discriminator: 'group',
        controls: {
            date: {
                discriminator: 'control',
                initValue: yesterday(),
                control: {
                    discriminator: 'datepicker',
                    type: 'date',
                    min: new Date(2020, 0, 22, 0, 0, 0),
                    max: yesterday()
                }
            }
        }
    };

    public paginationConfig: PaginationRequestModel = {
        pageSize: 500,
        pageIndex: 0
    };

    public filtersFormGroup: FormGroup;

    public isLoading = false;

    constructor(protected covidStatisticsService: CovidStatisticsService,
                protected dynamicFormService: DynamicFormService) {
        super();

        this.filtersFormGroup = this.dynamicFormService.generateForm(this.filtersQuestion);

        this.loaderConfigurator = new PaginatedListLoaderConfigurator<Country>({
            service: this.covidStatisticsService
        });
    }

    ngOnInit(): void {
        this.subs$.push(
            this.loaderConfigurator.onDataLoaded$.subscribe(
                data => {
                    this.isLoading = false;
                    this.data = data;
                },
                error => {
                    this.isLoading = false;
                    this.data = [];
                }
            ),
            this.filtersFormGroup.valueChanges.subscribe((values) => {
                this.loadData(values.date);
            })
        );

        this.loadData();
    }


    loadData(date = null) {
        if (!this.loaderConfigurator) {
            return;
        }
        this.isLoading = true;
        this.loaderConfigurator.loadData(this.getRequestParams(date));
    }

    getRequestParams(date = null): PaginatedRequestConfig {
        return {
            pagination: this.paginationConfig,
            filters: {
                date: this.dateStringForFilter(date)
            }
        };
    }

    /**
     * By default use yesterday, because daily statistics for today is not ready yet
     */
    private dateStringForFilter(date = null): string {
        if (date === null) {
            date = yesterday();
        }
        return formatDate(date, 'yyyy-MM-dd', 'en');
    }
}
