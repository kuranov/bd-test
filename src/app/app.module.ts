import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TableModule} from '@bd-innovations/dynamic-tables';
import {HttpClientModule} from '@angular/common/http';
import {TranslateModule} from '@ngx-translate/core';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatCardModule} from '@angular/material/card';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {CovidStatsTableComponent} from './covid-statistics-table/covid-statistics-table.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {DynamicFormModule} from '@bd-innovations/dynamic-form';
import {MatIconModule} from '@angular/material/icon';

@NgModule({
    declarations: [
        AppComponent,
        CovidStatsTableComponent
    ],
    imports: [
        MatSidenavModule,
        MatListModule,
        MatToolbarModule,
        MatCardModule,
        MatProgressBarModule,
        MatIconModule,
        DynamicFormModule,
        TableModule.forRoot({
            globalIcons: {
                actions: 'mdi mdi-dots-vertical',
                bulkActions: 'mdi mdi-dots-horizontal',
                filter: 'mdi mdi-filter',
                expand: 'mdi mdi-chevron-down',
                displayedColumns: 'mdi mdi-view-parallel',
                displayedColumnsReorder: 'mdi mdi-drag-horizontal-variant',
                removeChip: 'mdi mdi-close-circle-outline',
            },
            actionIcons: {
                open: {iconClass: 'mdi mdi-dots-horizontal', label: 'Details'},
                edit: {iconClass: 'mdi mdi-pencil', label: 'Edit'},
                delete: {iconClass: 'mdi mdi-delete', label: 'Delete'},
                download: {iconClass: 'mdi mdi-download', label: 'Download'},
            },
            paginatorPageSizeOptions: [500],
            globalTranslatePrefix: '',
            nullOrUndefinedPipePlaceholder: '-'
        }),
        HttpClientModule,
        TranslateModule.forRoot(),
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
